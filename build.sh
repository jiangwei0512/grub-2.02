##
#  @Package     : GRUB
#  @FileName    : build.sh
#  @Date        : 20181002
#  @Author      : Wei Jiang
#  @Version     : 1.0
#  @Description :
#    Generate bootx64.efi for UEFI use.
#    If there is error about automake, just use "touch *".
#
#  @History:
#    20181002: Initialize.
#    20190810: Change the grub-mkimage command.
#
#  This program and the accompanying materials
#  are licensed and made available under the terms and conditions of the BSD License
#  which accompanies this distribution. The full text of the license may be found at
#  http://opensource.org/licenses/bsd-license.php
#
#  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
#  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
##

if [ ! -f Makefile ]
then
  ./configure --target=x86_64 --with-platform=efi
fi

make

if [ $? -eq 0 ]
then
  echo "make succeeded."
  echo "making bootx64.efi..."
  ./grub-mkimage -p /EFI/BOOT -d ./grub-core/ -O x86_64-efi -o bootx64.efi boot linux part_msdos part_gpt fat normal serial efi_gop minicmd
  if [ $? -eq 0 ]
  then
  echo "copying bootx64.efi ..."
    if [ -f ~/code/vUDK2018/LearnUefiPkg/bootx64.efi ]
    then
      cp bootx64.efi ~/code/vUDK2018/LearnUefiPkg/bootx64.efi
    fi
  fi
else
  echo "make failed."
fi
