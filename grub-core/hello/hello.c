/* hello.c - test module for dynamic loading */
/*
 *  GRUB  --  GRand Unified Bootloader
 *  Copyright (C) 2003,2007  Free Software Foundation, Inc.
 *  Copyright (C) 2003  NIIBE Yutaka <gniibe@m17n.org>
 *
 *  GRUB is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  GRUB is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with GRUB.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <grub/types.h>
#include <grub/misc.h>
#include <grub/mm.h>
#include <grub/err.h>
#include <grub/dl.h>
#include <grub/extcmd.h>
#include <grub/i18n.h>
// jiangwei-20190811-ProtocolTest-start>>
#ifdef GRUB_MACHINE_EFI
#include <grub/efi/efi.h>
#include <grub/efi/api.h>

static void efi_hello (void)
{
  grub_efi_uintn_t   num_handles;
  grub_efi_handle_t  *handles;
  grub_efi_handle_t  *handle;
  grub_efi_guid_t    guid = EFI_HELLO_WORLD_PROTOCOL_GUID;

  handles = grub_efi_locate_handle (GRUB_EFI_BY_PROTOCOL, &guid,
            0, &num_handles);
  if (!handles)
  {
    return;
  }

  for (handle = handles; num_handles--; handle++)
  {
    efi_hello_world_t *hello_p;
    hello_p = grub_efi_open_protocol (*handle, &guid,
              GRUB_EFI_OPEN_PROTOCOL_GET_PROTOCOL);
    if (hello_p)
    {
      efi_call_1 (hello_p->hello, hello_p);
    }
  }

  return;
}
#endif
// jiangwei-20190811-ProtocolTest-end<<

GRUB_MOD_LICENSE ("GPLv3+");

static grub_err_t
grub_cmd_hello (grub_extcmd_context_t ctxt __attribute__ ((unused)),
		int argc __attribute__ ((unused)),
		char **args __attribute__ ((unused)))
{
  grub_printf ("%s\n", _("Hello World"));

// jiangwei-20190811-ProtocolTest-start>>
#ifdef GRUB_MACHINE_EFI
  efi_hello ();
#endif
// jiangwei-20190811-ProtocolTest-end<<

  return 0;
}

static grub_extcmd_t cmd;

GRUB_MOD_INIT(hello)
{
  cmd = grub_register_extcmd ("hello", grub_cmd_hello, 0, 0,
			      N_("Say `Hello World'."), 0);
}

GRUB_MOD_FINI(hello)
{
  grub_unregister_extcmd (cmd);
}
